Para el desarrollo de la prueba, se usaron las siguentes librerias y componentes:

- Eclipse 2019-06 (4.12.0)
- JDK 1.8.0_161
- Spring Boot
- Gson, para el trabajo con JSON
- Base de datos H2 para el uso de la base de datos en memoria.
- Hibernet
- Gradle para las dependencias de librerias

La aplicacion se levanta ejecutando la clase 
BciMsCreateUserApplication, para las pruebas se uso POSTMAN como cliente REST

1) El endpoint sin autenticar de la aplicación es el:

- http://localhost:8080/api/user
 metodo POST
 
 Crea un usuario nuevo en la base de datos. Este es el JSON que se uso durante las pruebas:
 
 {
"name":"Manuel",
"password":"Manuel123",
"email":"m@g5.cl",
"phones":[{
	"number": "123456",
	"citycode": "1",
	"countrycode": "56"
	
}]
}

este JSON se debe poner el BODY de la peticion HTTP y 

2) El enpoint con autenticacion es el:
- http://localhost:8080/api/users
 metodo GET

 
 Retorna la lista de los usuarios en la base de datos, pero hay que pasar el token generado en el punto 1