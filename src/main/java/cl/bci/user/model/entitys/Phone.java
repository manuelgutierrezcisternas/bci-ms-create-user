package cl.bci.user.model.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "phone")
@Getter
@Setter
public class Phone {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long phone_id;
	
	@Column(name = "number")
	private String number;
	
	@Column(name = "citycode")
	private String citycode;
	
	@Column(name = "countrycode")
	private String countrycode;
	
	@ManyToOne
    @JoinColumn(name="id")
    private User user;
}
