package cl.bci.user.exceptions;

public class ExistEmailException extends Exception{
	
/**
	 * 
	 */
	private static final long serialVersionUID = -7507552034644618886L;
	private final String msj;
	
	public ExistEmailException (String message) {
		super (message);
		this.msj="";
		
	}	
	
	public ExistEmailException (Exception e) {
		super (e);
		this.msj=e.getMessage();
	}
}
