package cl.bci.user.exceptions;

public class BadPasswordException extends Exception{
	
/**
	 * 
	 */
	private static final long serialVersionUID = -7507552034644618886L;
	private final String msj;
	
	public BadPasswordException (String message) {
		super (message);
		this.msj="";
		
	}	
	
	public BadPasswordException (Exception e) {
		super (e);
		this.msj=e.getMessage();
	}
}
