package cl.bci.user.exceptions;

public class BadEmailException extends Exception{
	
/**
	 * 
	 */
	private static final long serialVersionUID = -7507552034644618886L;
	private final String msj;
	
	public BadEmailException (String message) {
		super (message);
		this.msj="";
		
	}	
	
	public BadEmailException (Exception e) {
		super (e);
		this.msj=e.getMessage();
	}
}
