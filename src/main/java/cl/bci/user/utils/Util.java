package cl.bci.user.utils;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import cl.bci.user.dto.UserDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Util {
	
	private Util() {}
	
	public static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	    Matcher mather = pattern.matcher(email);
	    return mather.find();
	}
	
	public static boolean isValidPassword(String password) {
		
		Pattern pattern = Pattern.compile("^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]{2}).*$");
	    Matcher mather = pattern.matcher(password);
	    return mather.find();
	}
	
	public static boolean isExistEmail(List<UserDTO> usersDto, String email) {
		boolean found = false;
		for (UserDTO user : usersDto) {
			if (user.getEmail().equals(email)) {
				found = true;
				break;
			}
		}
		return found;
	}
	
	public static String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("bciTestJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
						.map(GrantedAuthority::getAuthority) 
						.collect(Collectors.toList()))
						.setIssuedAt(new Date(System.currentTimeMillis()))
						.setExpiration(new Date(System.currentTimeMillis() + 600000))
						.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}
