package cl.bci.user.utils;

import java.util.List;

import cl.bci.user.dto.UserDTO;
import cl.bci.user.exceptions.BadEmailException;
import cl.bci.user.exceptions.BadPasswordException;
import cl.bci.user.exceptions.ExistEmailException;

public class Validate {
	
	private Validate() {}
	
	public static boolean validateRequest(UserDTO userDto, List<UserDTO> usersDto) throws BadEmailException, ExistEmailException, BadPasswordException{
		
		
		if (!Util.isValidEmail(userDto.getEmail())) {
			throw new BadEmailException("Email is not valid..." + userDto.getEmail());
		}
		
		if (Util.isExistEmail(usersDto, userDto.getEmail())) {
			throw new ExistEmailException("Email exist..." + userDto.getEmail());
		}
		
		if (!Util.isValidPassword(userDto.getPassword())) {
			throw new BadPasswordException("Password is not valid: min 8 chars, min 2 digits, min 1 char in uppercase..." + userDto.getPassword());
		}
		
		return true;
		
	}
	
	
	

}
