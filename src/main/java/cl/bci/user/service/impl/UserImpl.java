package cl.bci.user.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.bci.user.dto.UserDTO;
import cl.bci.user.model.entitys.IUserRepository;
import cl.bci.user.model.entitys.User;
import cl.bci.user.service.UserService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserImpl implements UserService{

	
	@Autowired
	private IUserRepository iUserRepository;
	
	
	@Override
	public UserDTO setUser(UserDTO userDto){
		User user = new User();
		user.setEmail(userDto.getEmail());
		user.setName(userDto.getName());
		user.setPassword(userDto.getPassword());
		user.setModified(new Date());
		user.setCreated(new Date());
		user.setLastLogin(new Date());
		user = iUserRepository.save(user);
		log.info("Data saved...");
		userDto.setUuid(user.getId());
		userDto.setCreated(user.getCreated());
		userDto.setModified(user.getModified());
		userDto.setLastLogin(user.getLastLogin());
		return userDto;
		
	}
	
	@Override
	public List<UserDTO> getUsers() {

		List<UserDTO> usersDto = new ArrayList<>();
		List<User> users = iUserRepository.findAll();
		log.info("Users list size..." + users.size());
		for (User user : users) {
			UserDTO userDto = new UserDTO();
			userDto.setUuid(user.getId());
			userDto.setEmail(user.getEmail());
			userDto.setName(user.getName());
			userDto.setPassword(user.getPassword());
			userDto.setCreated(user.getCreated());
			userDto.setModified(user.getModified());
			userDto.setLastLogin(user.getLastLogin());
			userDto.setToken(user.getToken());
			usersDto.add(userDto);
		}
		
		return usersDto;
		
	}
	
}
