package cl.bci.user.service;

import java.util.List;

import cl.bci.user.dto.UserDTO;

public interface UserService {
	
	public UserDTO setUser(UserDTO userDto);
	public List<UserDTO> getUsers();

}
