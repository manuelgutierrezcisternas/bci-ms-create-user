package cl.bci.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import cl.bci.user.dto.StatusDTO;
import cl.bci.user.dto.UserDTO;
import cl.bci.user.exceptions.BadEmailException;
import cl.bci.user.exceptions.BadPasswordException;
import cl.bci.user.exceptions.ExistEmailException;
import cl.bci.user.service.UserService;
import cl.bci.user.utils.Util;
import cl.bci.user.utils.Validate;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class CreateUserController {
	
	@Autowired
	UserService userService;
	
	/**
	 * Create a user
	 * @param userDto
	 * @return
	 */
	@PostMapping(value = "/user", produces = "application/json")
	public ResponseEntity<StatusDTO> createUser(@Valid @RequestBody UserDTO userDto){
		
		log.info("post request : " + userDto);
		StatusDTO status = null;
		HttpStatus httpStatus = null;
		try {
			Validate.validateRequest(userDto, userService.getUsers());
			userDto.setToken(Util.getJWTToken(userDto.getName()));
			userDto = userService.setUser(userDto);
			return new ResponseEntity<>(getStatusDTO(HttpStatus.OK.toString(), new Gson().toJson(userDto)), HttpStatus.OK);
			
		} catch (BadEmailException | BadPasswordException e) {			
			status = getStatusDTO(HttpStatus.BAD_REQUEST.toString(), e.getMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
			log.error(e.getMessage(), e);
		} catch (ExistEmailException e) {
			status = getStatusDTO(HttpStatus.FOUND.toString(), e.getMessage());
			httpStatus = HttpStatus.FOUND;
			log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(status, httpStatus);
	}
	
	/**
	 * Get all users
	 * @return
	 */
	@GetMapping(value = "/users", produces = "application/json")
	public ResponseEntity<StatusDTO> getUser(){
		
		List<UserDTO> usersDto = userService.getUsers();	
		usersDto.forEach(user -> log.info(user.toString()));

		if (usersDto == null || usersDto.isEmpty()) {
			return new ResponseEntity<>(getStatusDTO(HttpStatus.BAD_REQUEST.toString(), "Users not found..."), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(getStatusDTO(HttpStatus.OK.toString(), new Gson().toJson(usersDto)), HttpStatus.OK);
	}
	
	/**
	 * Build the status 
	 * @param code
	 * @param message
	 * @return
	 */
	private StatusDTO getStatusDTO(String code, String message) {
		StatusDTO statusDTO = new StatusDTO();
		statusDTO.setCode(code);
		statusDTO.setMessage(message);		
		return statusDTO;
	}
	
}
