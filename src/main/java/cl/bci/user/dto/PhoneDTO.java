package cl.bci.user.dto;

import lombok.Data;

@Data
public class PhoneDTO {
	
	private String number;
	private String citycode;
	private String countrycode;

}
