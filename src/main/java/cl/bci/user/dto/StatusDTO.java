package cl.bci.user.dto;

import lombok.Data;

@Data
public class StatusDTO {
	
	private String code;
	private String message;

}
