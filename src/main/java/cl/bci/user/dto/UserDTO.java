package cl.bci.user.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class UserDTO {
	
	private Long uuid;
	private String name;
	private String email;
	private String password;
	private Date created;
	private Date modified;
	private Date lastLogin;
	private String token;
	private List<PhoneDTO> phones;

}
