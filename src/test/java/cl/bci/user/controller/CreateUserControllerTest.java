package cl.bci.user.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import cl.bci.user.dto.StatusDTO;
import cl.bci.user.dto.UserDTO;
import cl.bci.user.exceptions.BadEmailException;
import cl.bci.user.exceptions.BadPasswordException;
import cl.bci.user.exceptions.ExistEmailException;

@RunWith(MockitoJUnitRunner.class)
public class CreateUserControllerTest {

	@InjectMocks
	private CreateUserController createUserController;
	
	@Before
	public void before() {
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreateUserOk() {
		
		Mockito.doReturn(new ResponseEntity<>(Mockito.any(UserDTO.class), HttpStatus.OK)).when(createUserController).createUser(Mockito.any(UserDTO.class));
		ResponseEntity<StatusDTO> actual = createUserController.createUser(new UserDTO());
		assertEquals(HttpStatus.OK.toString(), actual.getBody().getCode());
	}
	
	@Test(expected = BadEmailException.class)
	public void testCreateUserBadEmailException() {
		Mockito.doThrow(BadEmailException.class).when(createUserController).createUser(Mockito.any(UserDTO.class));
		createUserController.createUser(new UserDTO());
	}
	
	@Test(expected = ExistEmailException.class)
	public void testCreateUserExistEmailException() {
		Mockito.doThrow(ExistEmailException.class).when(createUserController).createUser(Mockito.any(UserDTO.class));
		createUserController.createUser(new UserDTO());
	}
	
	@Test(expected = BadPasswordException.class)
	public void testCreateUserBadPasswordException() {
		Mockito.doThrow(BadPasswordException.class).when(createUserController).createUser(Mockito.any(UserDTO.class));
		createUserController.createUser(new UserDTO());
	}
	
}
